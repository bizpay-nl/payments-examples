# Example integrations

To implement the bizpay api you can use this SDK.

Example implementations can be found in the examples directory.

Documentation on the API can be found here: https://payments.bizpay.nl/swagger/

## Installation

Include this package by running

`composer require bizpay/payments`

## Get transaction URL

Code to implement the transaction api looks as follows:

```php
$transaction = new \BizPay\Payments\Transaction();
$transaction->setApiKey("api key");
$transaction->setPassword("password");

$data = [
    "webshop_customer_info" => [
        "company_identification" => "30172890",
        "company_name" => "Test",
        "firstname" => "Test",
        "surname_1" => null,
        "surname_2" => "Test",
        "title" => null,
        "email" => "Test",
        "phone" => "Test",
        "street" => "Test",
        "number" => "Test",
        "extension" => null,
        "city" => "Test",
        "postcode" => "Test",
        "state" => null,
        "country" => "NL",
        "gender" => 1
    ],
    "order_details" => [
        "order_created_date" => date('d-m-Y h:i'),
        "webshop_redirect_url" => "http://localhost/bizpay_apicall/response.php",
        "webshop_webhook_url" => 'http://localhost/bizpay_apicall/response.php',
        "currency" => "eur",
        "total_amount" => "100.00",
        "order_internal_id" => "25",
        "order_tax_amount" => "0.00",
        "shipment_amount" => "5.00",
        "shipment_tax_amount" => "0.00",
        "shipment_with_tax" => "5.00",
        "items" => []
    ]
];

try {
    $response = $transaction->create($data);

    echo $response->transaction_url;
} catch (Exception $e) {
    // An error occured, check for specific exceptions and handle them gracefully
}

```