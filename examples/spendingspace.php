<?php

require_once("vendor/autoload.php");

$spendingspace = new \BizPay\Payments\Spendingspace();
$spendingspace->setApiKey("api key");
$spendingspace->setPassword("password");

$data = [
    "chamber_of_commerce_number" => "30172890"
];

try {
    $response = $spendingspace->check($data);
    var_dump($response);
} catch (Exception $e) {
    // An error occured, check for specific exceptions and handle them gracefully
}




