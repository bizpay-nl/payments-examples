<?php

use PHPUnit\Framework\TestCase;

class PaymentEncryptionTest extends TestCase
{
    public function testEncrypt()
    {
        $data = ['key' => 'value'];
        $password = 'testPassword';

        $payments = new \BizPay\Payments();
        $payments->setPassword($password);

        $retval = $payments->encrypt($data);

        $this->assertGreaterThan(0, strlen($retval), 'encrypted body cannot be empty');
    }
}