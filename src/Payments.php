<?php

namespace BizPay;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\EnvironmentIsBrokenException;

class Payments
{

    /**
     * @var string API Key
     */
    private $apiKey = '';

    /**
     * @var string Password
     */
    private $password = '';

    /**
     * @var string Backend base url
     */
    private $url = 'https://payments.bizpay.nl';

    /**
     * @var int
     */
    private $timeout = 500;

    /**
     * @param mixed $data
     * @return string
     * @throws EnvironmentIsBrokenException
     */
    public function encrypt($data)
    {
        return Crypto::encryptWithPassword(json_encode($data), $this->getPassword(), true);
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }


}