<?php

namespace BizPay\Payments;

use BizPay\Payments;
use GuzzleHttp\Client;

class Spendingspace extends Payments
{
    /**
     * @param array|object $data
     * @return object
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function check($data)
    {
        $payload = $this->encrypt($data);

        $client = new Client([
            'timeout' => $this->getTimeout()
        ]);

        $res = $client->request('POST', $this->getUrl() . '/spendingspace', [
            'headers' => [
                'X-Authorization' => $this->getApiKey()
            ],
            'body' => $payload,
        ]);

        return json_decode($res->getBody()->getContents());
    }
}