<?php

namespace BizPay\Payments;

use BizPay\Payments;
use GuzzleHttp\Client;

class Transaction extends Payments
{
    /**
     * @param array|object $data
     * @return object
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($data)
    {
        $payload = $this->encrypt($data);

        $client = new Client([
            'timeout' => $this->getTimeout()
        ]);

        $res = $client->request('POST', $this->getUrl() . '/transaction', [
            'headers' => [
                'X-Authorization' => $this->getApiKey()
            ],
            'body' => $payload,
        ]);

        return json_decode($res->getBody()->getContents());
    }
}